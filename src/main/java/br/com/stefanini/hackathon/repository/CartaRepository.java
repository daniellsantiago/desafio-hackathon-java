package br.com.stefanini.hackathon.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.stefanini.hackathon.model.Carta;

public interface CartaRepository extends JpaRepository<Carta, Long> {

}
