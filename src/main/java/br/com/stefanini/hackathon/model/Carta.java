package br.com.stefanini.hackathon.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id; 

@Entity
public class Carta {
	
	@Id
	private Long id;
	
	@Column
	private String mensagem;
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}	

	public String getMensagem() {
		return mensagem;
	}
	
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	
}
