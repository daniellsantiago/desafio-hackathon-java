package br.com.stefanini.hackathon.resource;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.stefanini.hackathon.model.Carta;
import br.com.stefanini.hackathon.repository.CartaRepository;

@RestController
@RequestMapping("cartas")
public class CartaResource {	
	
	private CartaRepository repository;
	
	@PutMapping
	public ResponseEntity<?> enviar(Carta c){		
		return ResponseEntity.ok(repository.save(c));
	}
	
	@GetMapping
	public ResponseEntity<?> consultar(){
		return ResponseEntity.ok(repository.findAll());
	}

}
