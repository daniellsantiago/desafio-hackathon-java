package br.com.stefanini.hackathon;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@EnableAutoConfiguration
@SpringBootApplication
@ComponentScan(basePackages = "br.com.stefanini.hackathon")
public class DesafioHackathonApplication {

	public static void main(String[] args) {
		SpringApplication.run(DesafioHackathonApplication.class, args);
	}

}
